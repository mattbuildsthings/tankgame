package com.tankgame;

public class Player {
	private int playerX, playerY;
	private double x1,x2,y1,y2,theta;
	
	Player(int xaxis, int yaxis){
		playerX=xaxis;
		playerY=yaxis;
	}
	
	boolean collision(double xaxis, double yaxis, double height, double length, double angle)
	{		
		double Ax=playerX, Ay=playerY, Bx=playerX, By=playerY+height, Cx=playerX+length, Cy=playerY+height, Dx=playerX+length, Dy=playerY;
		double AxChange, AyChange, BxChange, ByChange, CxChange, CyChange, DxChange, DyChange, x1, y1, x2, y2, xOffset,y1offset,y2offset;
		double theta=angle;
		
		AxChange = Ax*(Math.cos((Math.toRadians(theta)))) - Ay*(Math.sin((Math.toRadians(theta))));
		AyChange = Ax*(Math.sin((Math.toRadians(theta)))) + Ay*(Math.cos((Math.toRadians(theta))));
		BxChange = Bx*(Math.cos((Math.toRadians(theta)))) - By*(Math.sin((Math.toRadians(theta))));
		ByChange = Bx*(Math.sin((Math.toRadians(theta)))) + By*(Math.cos((Math.toRadians(theta))));
		CxChange = Cx*(Math.cos((Math.toRadians(theta)))) - Cy*(Math.sin((Math.toRadians(theta))));
		CyChange = Cx*(Math.sin((Math.toRadians(theta)))) + Cy*(Math.cos((Math.toRadians(theta))));
		DxChange = Dx*(Math.cos((Math.toRadians(theta)))) - Dy*(Math.sin((Math.toRadians(theta))));
		DyChange = Dx*(Math.sin((Math.toRadians(theta)))) + Dy*(Math.cos((Math.toRadians(theta))));
		
		if (AxChange>=BxChange){
			x1=CxChange-BxChange;
			x2=DxChange-CxChange;
			xOffset=AxChange-BxChange;
			y1offset=ByChange-AyChange;
			y2offset=DyChange-AyChange;
			
			
		}
		else{
			theta=180-(90-angle);
			
			x1=BxChange-AxChange;
			x2=CxChange-BxChange;
			xOffset=0;
			y1offset=0;
			y2offset=CyChange-AyChange;
		}
		
		if (xaxis>(playerX-xOffset) && xaxis<(playerX-xOffset+x1) && yaxis<= ((xaxis+xOffset-playerX)*(Math.tan((Math.toRadians(theta))))+playerY+y1offset)){
			return true;
		}
		if (xaxis>(playerX-xOffset+x1) && xaxis<(playerX-xOffset+x1+x2) && yaxis<=(((playerX+x1+x2-xaxis-xOffset)/(Math.tan((Math.toRadians(theta)))))+playerY+y2offset)){
			System.out.print("Hit2");
			return true;
		}
		else
			return false;
	}
	
	void fire()
	{
		
	}
	
	int getXAxis(){
		return playerX; 
	}
	
	int getYAxis(){
		return playerY;
	}
}
