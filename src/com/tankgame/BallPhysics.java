package com.tankgame;

import com.badlogic.gdx.Gdx;

public class BallPhysics {
	public double vertSpeed;
	public double horSpeed;
	final double gravity = -.02;
	
	public BallPhysics(){
		
	}
	
	public void setIntSpeed(double speed, double angle){
		vertSpeed = speed*(Math.sin(Math.toRadians(angle)));
		horSpeed = speed*(Math.cos(Math.toRadians(angle)));
		
		System.out.print("Vert Speed: " + vertSpeed);
		System.out.print("Hor Speed: " + horSpeed);
		System.out.print("Angle: " + angle);
	}
	
	public double setVertSpeed(double yaxis){
		
		vertSpeed=vertSpeed + gravity; 
		
		yaxis += vertSpeed;
		return yaxis;
	}
	
	public double getHorSpeed(double xaxis){
		xaxis += horSpeed;

		return xaxis;
	}
	
	
}
