package com.tankgame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Random;

public class Game implements ApplicationListener {
	private Random randomGen = new Random();
	private SpriteBatch batch;
	private Texture cannonball, grass, tank, left, right;
	double speed = 4.75; // 50 pixels per second.
	double angle = 75.00;
	float tankAngle, player1Angle, player2Angle;
	double vertSpeed, horSpeed;
	boolean speedSet;
	double xaxis, yaxis;
	int[] levelHeight = new int[800];
	BallPhysics physics = new BallPhysics();
	Player player1, player2;
	int heightInc;
	double player1x, player1y, player2x, player2y, player1Level, player2Level;
	boolean player1turn, player2turn, fire;



	@Override
	public void create() {
		batch = new SpriteBatch();
		cannonball = new Texture(Gdx.files.internal("cannonball.png"));
		grass = new Texture(Gdx.files.internal("grass.png"));
		tank = new Texture(Gdx.files.internal("tank.png"));
		left = new Texture(Gdx.files.internal("left.png"));
		right = new Texture(Gdx.files.internal("right.png"));
		tankAngle =0;  //needs to be set with actual angle automatically 
		//vertSpeed = speed;
		speedSet=false;
		fire =false;

		//Builds the hills for the level
		levelHeight[0]=200;
		for (int i=1; i<800; i++){
			if (i<800)
				heightInc=1;
			if (i<670)
				heightInc=0;	
			if (i<620)
				heightInc=-1;
			if (i<400)
				heightInc=0;
			if (i<300)
				heightInc=1;
			if (i<140)
				heightInc=0;
			if (i<75)
				heightInc=-1;
			levelHeight[i]=(levelHeight[i-1]+heightInc);
		}
		
		//Generates a random placement for each tank
		int player1x=100;//randomGen.nextInt(250)+50;
		int player2x=605;//randomGen.nextInt(250)+500;
		player1 = new Player(player1x,levelHeight[player1x]);
		player2 = new Player(player2x,levelHeight[player2x]);
		
		//Initial placement of ball
		xaxis=player1.getXAxis();
		yaxis=player1.getYAxis();
		player1turn=false;
		player2turn=false;
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {

	if(fire==false && Gdx.input.isKeyPressed(Keys.DPAD_RIGHT))
		player1turn=true;
	if(fire==false &&Gdx.input.isKeyPressed(Keys.DPAD_LEFT))
		player2turn=true;
	
	if (player1turn==true){		
		if (speedSet==false){
			fire=true;
			physics.setIntSpeed(speed,(angle+tankAngle));	
			speedSet=true;
			xaxis=player1.getXAxis();
			yaxis=player1.getYAxis();
		}
				
		for (int ang=90; ang>=-90; ang--){
		//int ang=45;	
		player1x= (float) (32*(Math.cos((Math.toRadians(ang)))));
		player1y= (float) (32*(Math.sin((Math.toRadians(ang)))));
		player1Level=levelHeight[(int) (player1.getXAxis()+player1x)];
		player2Level=levelHeight[(int) (player2.getXAxis()+player1x)];
		if((player1.getYAxis()+player1y)>=player1Level){
			player1Angle=ang;
		 }
		if((player2.getYAxis()+player1y)>=player2Level){
			player2Angle=ang;
		 }
		}
		
		if ((player2.collision(xaxis,yaxis,8,32,player2Angle))==false && ((int)yaxis+1)>levelHeight[(int)xaxis]){
		yaxis=physics.setVertSpeed(yaxis);
		xaxis=physics.getHorSpeed(xaxis);
		}
		else{
			player1turn=false;
			fire=false;
			speedSet=false;
		}				
		
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
		batch.begin();
		batch.draw(tank, player1.getXAxis(), player1.getYAxis(), 1, 1, 32, 32, 1, 1, player1Angle, 0, 0, 32, 32, false, false); 
		batch.draw(tank, player2.getXAxis(), player2.getYAxis(), 1, 1, 32, 32, 1, 1, player2Angle, 0, 0, 32, 32, false, false);
		//batch.draw(tank, player2.getXAxis(),player2.getYAxis());
		for (int i=0; i<800; i++){
			int j=0;
			while (j<levelHeight[i]){
				batch.draw(grass, i,j);
				j=j+1;
			}
		}
		batch.draw(cannonball, (int)xaxis,(int)yaxis);
		batch.draw(left, 10,10);
		batch.draw(right, 100,10);
		batch.end();
	}
	if(player2turn==true){
		//System.out.print("GAME OVER");	
			if (speedSet==false){
				physics.setIntSpeed((speed*-1),((angle+tankAngle)*-1));	
				speedSet=true;
				xaxis=player2.getXAxis();
				yaxis=player2.getYAxis();
			}
					
			//player1x=32.0;
			//player2x=32.0;
			//player1y=(levelHeight[player1.getXAxis()+32])-(levelHeight[player1.getXAxis()]);
			//player2y=(levelHeight[player2.getXAxis()+32])-(levelHeight[player2.getXAxis()]);
			
			//player1Angle=(float) Math.toDegrees(Math.atan2(player1y, player1x)); 
			//player2Angle=(float) Math.toDegrees(Math.atan2(player2y, player2x)); 
			
			
			for (int ang=90; ang>=-90; ang--){
			//int ang=45;	
			player1x= (float) (32*(Math.cos((Math.toRadians(ang)))));
			player1y= (float) (32*(Math.sin((Math.toRadians(ang)))));
			player1Level=levelHeight[(int) (player1.getXAxis()+player1x)];
			player2Level=levelHeight[(int) (player2.getXAxis()+player1x)];
			if((player1.getYAxis()+player1y)>=player1Level){
				player1Angle=ang;
			 }
			if((player2.getYAxis()+player1y)>=player2Level){
				player2Angle=ang;
			 }
			}
			
			if ((player1.collision(xaxis,yaxis,8,32,player1Angle))==false && ((int)yaxis+1)>levelHeight[(int)xaxis]){
			yaxis=physics.setVertSpeed(yaxis);
			xaxis=physics.getHorSpeed(xaxis);
			}
			else{
				player2turn=false;
				fire=false;
				speedSet=false;
			}
				
			//}
			
			//System.out.print("X:" + player1x + "\n");
			//System.out.print("Y:" +player1y + "\n");
			//System.out.print("Level Height:" + (int)player1Level + "\n");
			//System.out.print("Sprite Height:" + (int)(player1.getYAxis()+player1y) + "\n");

			
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
			batch.begin();
			batch.draw(tank, player1.getXAxis(), player1.getYAxis(), 1, 1, 32, 32, 1, 1, player1Angle, 0, 0, 32, 32, false, false); 
			batch.draw(tank, player2.getXAxis(), player2.getYAxis(), 1, 1, 32, 32, 1, 1, player2Angle, 0, 0, 32, 32, false, false);
			//batch.draw(tank, player2.getXAxis(),player2.getYAxis());
			for (int i=0; i<800; i++){
				int j=0;
				while (j<levelHeight[i]){
					batch.draw(grass, i,j);
					j=j+1;
				}
			}
			batch.draw(cannonball, (int)xaxis,(int)yaxis);
			batch.draw(left, 10,10);
			batch.draw(right, 100,10);
			batch.end();
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
